import 'package:flutter/material.dart';
import 'package:routing_screen/user_info.dart';
import 'package:routing_screen/User.dart';

void main() => runApp(Login());

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      home: LoginPage(),
      theme: ThemeData(
        primarySwatch: Colors.blue
      ),
    );
  }
}


class LoginPage extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<LoginPage> with SingleTickerProviderStateMixin{
  AnimationController _iconAnimationController;
  Animation<double> _iconAnimation;
  TextEditingController _emailController = TextEditingController();
  TextEditingController _passwordController = TextEditingController();
  Color _styleColor = Colors.teal;
  final _formKey = GlobalKey<FormState>();
  bool _autoValidate = false;
  String _email;
  String _password;




  @override
  void initState() {
    super.initState();
    // TODO: implement initState
    _iconAnimationController = new AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 500)
    );
    
    _iconAnimation = new CurvedAnimation(parent: _iconAnimationController, curve: Curves.easeInOutSine);
    _iconAnimation.addListener(() => this.setState((){}));
    _iconAnimationController.forward();

  }


  @override
  void dispose() {
    // Clean up the controller when the Widget is removed from the Widget tree
    _emailController.dispose();
    super.dispose();
  }

  String validateEmail(String value) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  String validatePassword(String value) {
    if (value.length == 0) {
      return "Password is Required";
    } else if (value.length > 0 && value.length < 8) {
      return "Password at least 8 characters";
    }
    return null;
  }

  _handleLogin() {
    if (_formKey.currentState.validate()) {
      // No any error in validation
      _formKey.currentState.save();
      print("Email $_email");
      print("Password $_password");
      User user = User(email: _email, password: _password);
      _emailController.clear();
      _passwordController.clear();
      FocusScope.of(context).requestFocus(new FocusNode());
      setState(() {
        _autoValidate = false;
      });

      Navigator.push(context,
          MaterialPageRoute(
              builder: (context) => UserInfor(
                user: user,
              )
          )
      );
    } else {
      // validation error
      setState(() {
        _autoValidate = true;
      });
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.black,
        body: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            Image(image: AssetImage("assets/blue_landscape.jpg"),
            fit: BoxFit.cover,
            color: Colors.black54,
            colorBlendMode: BlendMode.darken,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlutterLogo(
                  size: _iconAnimation.value * 100,
                ),
                Form(
                  key: _formKey,
                  autovalidate: _autoValidate,
                  child: Theme(data: ThemeData(
                    brightness: Brightness.dark,
                    primarySwatch: _styleColor,
                    inputDecorationTheme: InputDecorationTheme(
                      labelStyle: TextStyle(
                        color: _styleColor, fontSize: 18.0,
                      )
                    )

                  ), child: Container(
                    padding: const EdgeInsets.all(40),
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          autofocus: false,
                          decoration: InputDecoration(
                            labelText: "Enter Email",
                          ),
                          keyboardType: TextInputType.emailAddress,
                          validator: validateEmail,
                          controller: _emailController,
                          onSaved: (String val) {
                            _email = val;
                          },

                        ),
                        SizedBox(height: 10.0),
                        TextFormField(
                          autofocus: false,
                          decoration: InputDecoration(
                            labelText: "Enter Password",
                          ),
                          keyboardType: TextInputType.text,
                          obscureText: true,
                          validator: validatePassword,
                          controller: _passwordController,
                          onSaved: (String val) {
                            _password = val;
                          },
                        ),
                        SizedBox(height: 30.0),
                        MaterialButton(
                          height: 40,
                          minWidth: 100,
                          color: _styleColor,
                          textColor: Colors.white,
                          child: Text("Login"),
                          splashColor: Colors.lightGreen,
                          onPressed: _handleLogin
                        )
                      ],
                    ),
                  )),
                )
              ],
            )
          ],
        ),
      );
  }
}

