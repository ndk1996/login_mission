import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:routing_screen/User.dart';


class UserInfor extends StatefulWidget {
  final User user;
  UserInfor({Key key, @required this.user}) : super(key: key);

  @override
  _UserInfor createState() => _UserInfor();
}

class _UserInfor extends State<UserInfor> {
  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      _image = image;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.teal,
        title: Text('User Infomation'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _image == null
                ? FlutterLogo(size: 100,)
                : Image(image: FileImage(_image), fit: BoxFit.cover, width: 200, height: 200,),
            SizedBox(height: 30),
            Text("Email: ${widget.user.email}", style: TextStyle(fontSize: 20, fontFamily: FontWeight.bold.toString()),),
          ],
        ),
      ),


      floatingActionButton: FloatingActionButton(
        onPressed: getImage,
        tooltip: 'Pick Image',
        child: Icon(Icons.add_a_photo),
        backgroundColor: Colors.teal,
      ),
    );
  }
}
